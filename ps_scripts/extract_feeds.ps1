﻿param (
#    [string]$AltaHRWebServiceUrl = "https://altahr-cws.bham.ac.uk/sharepoint-hr/",
#    [string]$AltaHRWebServiceUsername="sharepoint_hr_link",
#    [string]$AltaHRWebServicePassword = "IMm1GRUkVISp0n",
    [ValidateSet('live','test')][string]$web_svc = 'live',
    [ValidateSet('colleges', 'schools', 'staff', 'hr_admins', 'am_admins', 'admins', 'all')][string[]]$feeds = ('all')
)

Set-StrictMode -Version 1

$WEB_SERVICES = @{
    live = @{
        url = 'https://altahr-cws.bham.ac.uk/sharepoint-hr'
        username = 'sharepoint_hr_link'
        password = 'IMm1GRUkVISp0n'
    }
    test = @{
        url = 'https://altahr-cws.bham.ac.uk/sharepoint-hr-test'
        username = 'sharepoint_hr_link'
        password = 'a6dtks0ni'
    }
}

$FeedsDataset = @{
    colleges = @{ command = 'get_colleges';  data=$null };
    schools = @{ command = 'get_schools';  data=$null };
    staff = @{ command = 'get_immigration_staff'; data=$null }
    hr_admins = @{ command = 'get_hr_immigration_admins';  data=$null }
    am_admins = @{ command = 'get_immigration_am_admins'; data=$null }
    admins = @{ command = 'get_immigration_admins'; data=$null }
}

if ($feeds -contains 'all') {
    $FeedsToProcess = $FeedsDataset.Keys | % { $_ }
} else {
    $FeedsToProcess = $feeds
}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$creds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $WEB_SERVICES[$web_svc].username, $WEB_SERVICES[$web_svc].password)))

foreach ($ftr in $FeedsToProcess) {
    $f = $FeedsDataset[$ftr]

    $url = ("{0}/{1}" -f $WEB_SERVICES[$web_svc].url, $f.command) 
    try {
        $f.data = Invoke-RestMethod -Uri $url -Headers @{Authorization = "Basic $creds"} -TimeoutSec 300
    } catch {
        $_
    }
    $dummy = $true
}

$dummy = $true