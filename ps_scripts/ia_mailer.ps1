param (
    [Parameter(Mandatory=$true)][string]$SQL_Server,
    [ValidateSet('live', 'test', 'no_email', 'debug')][string[]]$mode = ('live'),
    [Parameter(Mandatory=$true)][string]$log_base,
    [int]$cutoff_interval,
    [string]$test_email = "c.romero@bham.ac.uk",
    [int]$test_sends = -1,
    [string]$project_officer_name = "Rajvinder Pawar",
    [string]$project_officer_ext = "51110",
    [string]$templates_folder = "EmailTemplates",
    [string]$SQL_db = "hr_immigration",
    [string]$mail_from = "staffimmigration@contacts.bham.ac.uk",
    [string[]]$mail_cc = @('hrservices@contacts.bham.ac.uk'),
    [string[]]$mail_bcc = @('N.Gibbons@bham.ac.uk', "c.romero@bham.ac.uk"),
    [string]$smtp_host = "smtp.bham.ac.uk",
    [int]$smtp_port = 25
)

Set-StrictMode -Version 1

function Import-SQLModule {
    Get-Command -Name Invoke-Sqlcmd -ErrorAction SilentlyContinue
    if ($? -eq $false) {

        Push-Location

        $local:SQLModule = Get-Module -name SQLServer -listavailable | select -first 1
        if ($null -eq $local:SQLModule) {
            $local:SQLModule = Get-Module -name SQLPS -listavailable | select -First 1
        }
        if ($null -ne $local:SQLModule) {
            $local:SQLModule | Import-Module -DisableNameChecking
        } 
     
        Pop-Location

        return ($local:SQLModule -ne $null)
    } else {
        return $true
    }

}

function get-NearestMonday {
    $dt = Get-Date

    $offset = switch ($dt.DayOfWeek) {
        [DayOfWeek]::Sunday { 1 }
        [DayOfWeek]::Saturday { 2 }
        default { ($dt.DayOfWeek.value__ - [DayOfWeek]::Monday.value__)*-1 }
    }

    return $dt.AddDays($offset)
}


$TEST_FOOTER = @"
    <p style="font-weight:bold;color:red;margin-top:6pt">Real recipient: {0}</p>
"@


$ScriptPath = Split-Path -Path $MyInvocation.MyCommand.Path -Parent

if (!(Test-Path $log_base -PathType Container)) {
    Write-Error "Invalid log base"
    exit 99
}

$LogFile = Join-Path -Path $log_base -ChildPath ("ia_emails_{0:yyyyMMdd}.log" -f (Get-Date))

if ($mode -contains 'test' -and [string]::IsNullOrEmpty($test_email)) {
    Write-Error "Test e-mail address required"
    exit 99
}

if ([System.IO.Path]::IsPathRooted($templates_folder)) {
    $TemplatesFolder = $templates_folder
} else {
    $TemplatesFolder = Join-Path -Path $ScriptPath -ChildPath $templates_folder
}

if (!(Test-Path $TemplatesFolder -PathType Container)) {
    Write-Error "Email Templates folder does not exist"
    exit 99
}

$HTMLTemplate = Join-Path -Path $TemplatesFolder -ChildPath "IAWeeklyAttendanceMoniitoring.html"
if (!(Test-Path $HTMLTemplate -PathType Leaf)) {
    Write-Error "Email Template does not exist"
    exit 99
}

$TemplateBody = (Get-Content -Path $HTMLTemplate | Out-String)

if (!(Import-SQLModule) ) {
    Write-Error "Could not load SQL Server module"
    exit 99
}

$AMA = Invoke-Sqlcmd -ServerInstance $SQL_Server -Database $SQL_db -Query "select username from ApplicationUsers where IsActive=1 and Role=1" -QueryTimeout 120 -ConnectionTimeout 30  -ErrorAction Stop | % {
    $cc = ([ADSISearcher]"cn=$($_.UserName)").FindOne()
    if (![string]::IsNullOrEmpty($cc.Properties['mail'])) { 
        $cc.Properties['mail']
    }
}
if ($null -eq $cutoff_interval -or $cutoff_interval -le 0) {
    $IncompleteAttendances = Invoke-Sqlcmd -ServerInstance $SQL_Server -Database $SQL_db -Query "EXEC New_Core.IAWeeklyAttendanceMoniitoring" -QueryTimeout 120 -ConnectionTimeout 30  -ErrorAction Stop
} else {
    $IncompleteAttendances = Invoke-Sqlcmd -ServerInstance $SQL_Server -Database $SQL_db -Query ("EXEC New_Core.IAWeeklyAttendanceMoniitoring @CutoffFrom='{0}'" -f $cutoff_interval) -QueryTimeout 120 -ConnectionTimeout 30  -ErrorAction Stop
}

$sent = 0
foreach ($StaffGrp in ($IncompleteAttendances | Group-Object -Property PayrollNo ) ) {    
    foreach ($incomplete in $StaffGrp.Group) {
        try {
            $recipient = ([ADSISearcher]"cn=$($incomplete.ContactUsername)").FindOne()
            if ([string]::IsNullOrEmpty($recipient.Properties['mail'])) { continue }

            $MailTo = @{$true=$test_email; $false=$recipient.Properties['mail']}[$mode -contains 'test']
            $MailCc = @{$true=@($test_email); $false=$mail_cc + $incomplete.HeadOfSchools + $AMA}[$mode -contains 'test']

            $body = $TemplateBody.Replace("{RECIPIENT}", $incomplete.ContactName).replace('{SCHOOL_NAME}', $incomplete.SchoolName).Replace('{REFERENCE_DATE}', $incomplete.WeekStartDate.toString('dd/MM/yyyy')).Replace('{PO_NAME}', $project_officer_name).Replace('{PO_EXT}', $project_officer_ext).Replace('{STAFF_MEMBER}', $incomplete.StaffMember)
            $subject = "IA - Weekly Attendance Monitoring for $($incomplete.StaffMember)"

            ("Sending attendance e-mail for {0} in {3} (w/c {2:dd/MM/yyyy}) to {1}" -f $incomplete.StaffMember, $incomplete.ContactName, $incomplete.WeekStartDate, $incomplete.SchoolName ) | % {
                if ($mode -contains 'debug' ) { Write-Host $_ }
                if ($sent -eq 0 ) {
                    Set-Content -Path $LogFile -Value $_
                } else {
                    Add-Content -Path $LogFile -Value $_
                }
            }

            if ($mode -notcontains 'no_email') {
                Send-MailMessage `
                    -To $MailTo -From $mail_from -Bcc $mail_bcc -Cc $MailCc `
                    -BodyAsHtml -Body $body -Subject $subject `
                    -SmtpServer $smtp_host
            }
            $sent++
            if ($mode -contains 'test') { break }
        } catch {
            $_
        }
   }
   if ($mode -contains 'test' -and $test_sends -ne -1 -and $sent -ge $test_sends) { break }
    
} 

$dummy = $true


