param (
    [Parameter(Mandatory=$true)][string]$SQL_Server,
    [ValidateSet('LeaveToRemainChecks', 'PersonalDetailsChecks')][string[]]$alerts = ('LeaveToRemainChecks', 'PersonalDetailsChecks'),
    [ValidateSet('prod', 'test_email', 'no_email', 'debug')][string[]]$mode = ('prod'),
    [string]$test_email = "c.romero@bham.ac.uk",
    [int]$test_sends = -1,
    [string]$project_officer_name = "Rajvinder Pawar",
    [string]$project_officer_ext = "51110",
    [string]$templates_folder = "EmailTemplates",
    [string]$SQL_db = "hr_immigration",
    [string]$mail_from = "staffimmigration@contacts.bham.ac.uk",
    [string[]]$mail_bcc = @('N.Gibbons@bham.ac.uk'),
    [string]$smtp_host = "smtp.bham.ac.uk",
    [int]$smtp_port = 25
)

Set-StrictMode -Version 1

function Import-SQLModule {
    Push-Location

    Import-Module -name SQLPS -DisableNameChecking
    
    $local:sqlmod = Get-Module -name SQLPS 
    if ($null -eq $local:sqlmod) {
        return $false
    }
    
    Pop-Location

    return $true
}

function Send-Alerts {
    param(
        [Parameter(Mandatory=$true)][string]$alert_type,
        [Parameter(Mandatory=$true)][string]$query,
        [Parameter(Mandatory=$true)][string]$template,
        [Parameter(Mandatory=$true)][string]$subject
    )

    $local:result = @{
        found = 0;
        sent = 0;
        success = $true;
        errors = @()
    }

    if ($script:mode -contains 'debug') {
        write-host "Starting $alert_type"
    }
    try {
        if (!(Test-Path -Path $template -PathType Leaf)) {
            throw ("Could not find $alert_type e-mail template")
        }
        if ($script:mode -contains 'debug') {
            write-host "Checking for staff members"
        }

        $local:staff = Invoke-Sqlcmd -ServerInstance $script:SQL_Server -Database $script:SQL_db -Query $query -QueryTimeout 120 -ConnectionTimeout 30  -ErrorAction Stop
        if ($script:mode -contains 'debug') {
            Write-Host ("{0} staff members found" -f $local:staff.count)
        }
        $local:result.found = $local:staff.count
        ForEach ($local:member in $local:staff) {
            try {
                if ($script:mode -contains 'debug') {
                    write-host ('Sending {3} alert to {0} {1} at {2}' -f $local:member.FirstName, $local:member.LastName, $local:member.EmailAddress, $alert_type )
                }
                if ($script:mode -notcontains 'no_email') {
                    $local:body = (Get-Content -Path $template | Out-String).replace('{NAME}', $local:member.FirstName+' '+$local:member.LastName).replace('{PO_NAME}', $script:project_officer_name).replace('{LTR_END_DATE}', ("{0:dd/MM/yyyy}" -f $local:member.LtrEndDate)).replace('{PO_EXT}', $script:project_officer_ext)
                    $local:mail_subject = $subject 
                    if ($script:mode -contains 'test_email') {
                        $local:body = $local:body.replace('</body>', ($TEST_FOOTER -f $local:member.EmailAddress)+'</body>')
                        $local:mail_to = $test_email
                        $local:mail_subject += ' - TEST E-MAIL'
                    } else {
                        $local:mail_to = $local:member.EmailAddress
                    }
                    Send-MailMessage -SmtpServer $script:smtp_host -Port $script:smtp_port `
                        -From $script:mail_from -To $local:mail_to  -Bcc $mail_bcc `
                        -Subject $mail_subject `
                        -BodyAsHtml -Body $local:body
                    $local:result.sent++
                    if ($script:mode -contains 'test_email' -and $local:result.sent -ge $test_sends -and $test_sends -ne -1) { break }
                }
            } catch {
                $result.success = $false;
                $local:result.errors += $error[0].Exception.Message
            }
        }
    } catch {
        $result.success = $false;
        $local:result.errors += $error[0].Exception.Message
    }

    if ($script:mode -contains 'debug') {
        write-host "Completed $alert_type"
    }
    return $result
}

$TEST_FOOTER = @"
    <p style="font-weight:bold;color:red;margin-top:6pt">Real recipient: {0}</p>
"@


$ScriptPath = Split-Path -Path $MyInvocation.MyCommand.Path -Parent

if ([System.IO.Path]::IsPathRooted($templates_folder)) {
    $TemplatesFolder = $templates_folder
} else {
    $TemplatesFolder = Join-Path -Path $ScriptPath -ChildPath $templates_folder
}

if (!(Test-Path $TemplatesFolder -PathType Container)) {
    Write-Error "Email Templates folder does not exist"
    exit 99
}

if (!(Import-SQLModule) ) {
    Write-Error "Could not load SQL Server module"
    exit 99
}


if ($alerts -contains 'PersonalDetailsChecks') {
    $result = Send-Alerts `
        -alert_type "Personal Details Check " `
        -template  (Join-Path -Path $TemplatesFolder -ChildPath "OutstandingPersonalDetailsCheck1.html") `
        -query "EXEC New_Core.GetStaffMembersThatHaveOutstandingPersonalDetailsChecks" `
        -subject ("Personal details checks are due as of {0:dd/MM/yyyy}" -f (Get-Date))

    if ($result.success -eq $false) {
        $result.errors | select -last 1
    }
}

if ($alerts -contains 'LeaveToRemainChecks') {
    $result = Send-Alerts `
        -alert_type "Leave To Remain Check" `
        -template  (Join-Path -Path $TemplatesFolder -ChildPath "LtrCheckDue.html") `
        -query "EXEC New_Core.GetStaffMembersThatHaveLtrCheckDue "  `
        -subject ("Leave to remain check is due as of {0:dd/MM/yyyy}" -f (Get-Date) )

    if ($result.success -eq $false) {
        $result.errors | select -last 1
    }
}