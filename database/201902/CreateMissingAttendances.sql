DECLARE new_staff_cursor CURSOR 
FOR
SELECT 
	ID, 
	SchoolId, 		
	AppointmentStartDate, 
	AttendanceStartDate,
	AppointmentEndDate
FROM StaffMembers
WHERE ImmigrationStatus IN (1,2)
AND IsActive=1 AND isApproved=1
AND (DATEDIFF(d, AppointmentStartDate, GETDATE()) between 0 and 14 OR DATEDIFF(d, AttendanceStartDate, GETDATE()) BETWEEN 0 AND 14);

DECLARE @vStaffMemberId int, @vSchoolId int, @vAppointmentStart date, @vAttendanceStart date, @vAppointmentEnd date;
-- DECLARE @vPayrollNo int, @vFirstName nvarchar(100), @vLastName nvarchar(100);
DECLARE @vWeekStartReference date, @ApptStartMonday date;
DECLARE @dow tinyint;

DECLARE @staff_member_attendances TABLE (StaffMemberId int, AttendanceId int, AttendanceType tinyint);
DECLARE @staff_member_attendance_completion TABLE ([DayOfWeek] tinyint, StaffMemberId int, AttendanceId int, IsComplete bit);

-- BEGIN TRAN

OPEN new_staff_cursor;

WHILE 1=1
BEGIN
	FETCH NEXT FROM new_staff_cursor INTO @vStaffMemberId, @vSchoolId,  @vAppointmentStart, @vAttendanceStart, @vAppointmentEnd;
	IF @@FETCH_STATUS<>0 BREAK;

	SET @ApptStartMonday = 	dbo.fnNearestMonday(@vAppointmentStart);

	SET @vWeekStartReference = CASE WHEN @ApptStartMonday<@vAttendanceStart THEN @ApptStartMonday ELSE @vAttendanceStart END;

	INSERT INTO @staff_member_attendances (StaffMemberId, AttendanceId, AttendanceType)
	SELECT @vStaffMemberId, att.id AS AttendanceId, 4
	FROM Attendances att
	WHERE att.SchoolId=@vSchoolId
	AND att.WeekStartDate BETWEEN @vWeekStartReference AND ISNULL(@vAppointmentEnd, '2099-12-31')
	AND NOT EXISTS(
		SELECT 0 FROM StaffMemberAttendances sma
		WHERE AttendanceId=ATT.Id and StaffMemberId=@vStaffMemberId
	);

	SET @dow=1

	WHILE @dow<6
	BEGIN
		INSERT INTO @staff_member_attendance_completion
		([DayOfWeek], StaffMemberId, AttendanceId, IsComplete)
		SELECT @dow, @vStaffMemberId, att.id, CASE WHEN DATEADD(d, @dow-1, WeekStartDate)<@vAppointmentStart THEN 1 ELSE 0 END
		FROM Attendances att
		WHERE att.SchoolId=@vSchoolId
		AND att.WeekStartDate BETWEEN @vWeekStartReference AND ISNULL(@vAppointmentEnd, '2099-12-31')
		AND NOT EXISTS(
			SELECT 0 FROM StaffMemberAttendances sma
			WHERE AttendanceId=ATT.Id and StaffMemberId=@vStaffMemberId
		);

		SET @dow=@dow+1;
	END
END

CLOSE new_staff_cursor;

DEALLOCATE new_staff_cursor;

SELECT stf.PayrollNo, stf.FirstName, stf.LastName, att.WeekStartDate, sma.*
FROM StaffMembers stf
INNER JOIN @staff_member_attendances sma
ON stf.id=sma.StaffMemberId
INNER JOIN Attendances att
ON att.id=sma.AttendanceId
ORDER BY sma.StaffMemberId, sma.AttendanceId;

SELECT stf.PayrollNo, stf.FirstName, stf.LastName, att.weekstartdate, ac.*
FROM StaffMembers stf
INNER JOIN @staff_member_attendance_completion ac
ON stf.id=ac.StaffMemberId
INNER JOIN Attendances att
ON att.id=ac.AttendanceId;


-- COMMIT TRAN;

