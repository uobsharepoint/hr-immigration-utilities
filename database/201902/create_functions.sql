IF OBJECT_ID('dbo.fnNearestMonday') IS NOT DULL DROP FUNCTION dbo.fnNearestMonday;
GO

CREATE FUNCTION dbo.fnNearestMonday (@dt date)
RETURNS date
AS
BEGIN
	DECLARE @monday date;

	SET @monday = CASE datepart(dw, @dt)
	WHEN 7 THEN DATEADD(dd, 2, @dt) -- following Monday
	ELSE DATEADD(dd, 2 - datepart(dw, @dt), @dt) -- nearest Monday
	END
	RETURN @monday;

END
GO

GRANT EXECUTE ON dbo.fnNearestMonday TO public;

GO
