USE HR_Immigration
GO

IF OBJECT_ID('dbo.vwStaffWithMissingAttendances') IS NOT NULL DROP VIEW dbo.vwStaffWithMissingAttendances;
GO


CREATE VIEW dbo.vwStaffWithMissingAttendances AS
SELECT 
	STF.id StaffMemberId, 
	stf.PayrollNo, 
    stf.FirstName, 
	stf.LastName, 
	CASE stf.ImmigrationStatus WHEN 1 THEN 'Tier 2' WHEN 2 THEN 'Tier 5' ELSE 'Unknown' END AS ImmigrationStatus, 
	sch.OrganisationDescription, 
	stf.AppointmentStartDate, 
	stf.AttendanceStartDate, 
	stf.AppointmentEndDate, 
	att.id AttendanceId, 
	att.WeekStartDate
FROM
(
	select  s.id,  s.schoolid, s.AppointmentEndDate,
		CASE 
		WHEN (ISNULL(AttendanceEndDate, '2099-12-31')<AppointmentStartDate OR DATEDIFF(d, AttendanceStartDate, AppointmentStartDate)>21)	-- school change ?
		THEN 
			CASE datepart(dw, AppointmentStartDate)
			WHEN 7 THEN DATEADD(dd, 2, AppointmentStartDate) -- following Monday
			ELSE DATEADD(dd, 2 - datepart(dw, AppointmentStartDate), AppointmentStartDate) -- nearest Monday
			END
		ELSE AttendanceStartDate
		END AS ApptNearestMonday
	from StaffMembers s
) as d1
INNER JOIN Attendances att
ON att.SchoolId=d1.SchoolId AND ATT.WeekStartDate between d1.ApptNearestMonday AND ISNULL(d1.AppointmentEndDate, '2099-12-31')
INNER JOIN StaffMembers stf
ON stf.id=d1.id
INNER JOIN Schools sch
ON sch.id=stf.SchoolId
AND stf.isActive=1 AND stf.ImmigrationStatus IN (1,2) AND stf.isApproved=1
AND NOT EXISTS(
	SELECT 0
	FROM StaffMemberAttendances sma
	WHERE sma.AttendanceId=att.id AND sma.StaffMemberId=d1.id
)
GO


