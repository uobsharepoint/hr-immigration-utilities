USE hr_immigration
GO

-- will eventually move procedure to dbo schema

IF OBJECT_ID('utils2018.sp_CreateMissingAttendanceForStaffMembers') IS NOT NULL DROP PROCEDURE utils2018.sp_CreateMissingAttendanceForStaffMembers;
GO

CREATE PROCEDURE utils2018.sp_CreateMissingAttendanceForStaffMembers(@cutoff smallint=14)
AS
BEGIN
	DECLARE new_staff_cursor CURSOR FOR
	SELECT 
		ID, 
		SchoolId, 		
		AppointmentStartDate, 
		AttendanceStartDate,
		AppointmentEndDate,
		AttendanceEndDate
	FROM StaffMembers
	WHERE ImmigrationStatus IN (1,2)
	AND IsActive=1 AND isApproved=1
	AND (DATEDIFF(d, AppointmentStartDate, GETDATE()) between 0 and @cutoff OR DATEDIFF(d, AttendanceStartDate, GETDATE()) BETWEEN 0 AND @cutoff);

	DECLARE @vStaffMemberId int, @vSchoolId int, @vAppointmentStart date, @vAttendanceStart date, @vAppointmentEnd date, @vAttendanceEnd date;
	DECLARE @vName nvarchar(200), @vPayrollNo int;
	DECLARE @vWeekStartReference date;
	DECLARE @RecordsAffected int;
	DECLARE @dow tinyint;

	DECLARE @retcode int=0;

	SET NOCOUNT ON;

	OPEN new_staff_cursor;
	
	BEGIN TRY
		WHILE 1=1
		BEGIN
			FETCH NEXT FROM new_staff_cursor INTO @vStaffMemberId, @vSchoolId,  @vAppointmentStart, @vAttendanceStart, @vAppointmentEnd, @vAttendanceEnd;
			IF @@FETCH_STATUS<>0 BREAK;

			SET @vWeekStartReference = CASE WHEN ISNULL(@vAttendanceEnd, '2099-12-31')<@vAppointmentStart THEN dbo.fnNearestMonday(@vAppointmentStart) ELSE dbo.fnNearestMonday(@vAttendanceStart) END;

			BEGIN TRAN;

			INSERT INTO dbo.StaffMemberAttendances(StaffMemberId, AttendanceId, IsLeaving, AttendanceType)
			SELECT @vStaffMemberId, att.id AS AttendanceId, 0, 4
			FROM Attendances att
			WHERE att.SchoolId=@vSchoolId
			AND att.WeekStartDate BETWEEN @vWeekStartReference AND ISNULL(@vAppointmentEnd, '2099-12-31')
			AND NOT EXISTS(
				SELECT 0 FROM StaffMemberAttendances sma
				WHERE AttendanceId=ATT.Id and StaffMemberId=@vStaffMemberId
			);

			SET @RecordsAffected = @@ROWCOUNT;

			IF (@RecordsAffected>0) 
			BEGIN
				SET @dow=1

				WHILE @dow<6
				BEGIN
					INSERT INTO dbo.StaffMemberAttendanceCompletionDatas
					([DayOfWeek], StaffMemberId, AttendanceId, IsComplete)
					SELECT @dow, @vStaffMemberId, att.id, CASE WHEN DATEADD(d, @dow-1, WeekStartDate)<@vAppointmentStart THEN 1 ELSE 0 END
					FROM Attendances att
					WHERE att.SchoolId=@vSchoolId
					AND att.WeekStartDate BETWEEN @vWeekStartReference AND ISNULL(@vAppointmentEnd, '2099-12-31')
					AND NOT EXISTS(
						SELECT 0 FROM StaffMemberAttendances sma
						WHERE AttendanceId=ATT.Id and StaffMemberId=@vStaffMemberId
					);

					SET @dow=@dow+1;
				END

				SELECT @vPayrollNo=PayrollNo, @vName=CONCAT(FirstName, ' ', LastName)
				FROM StaffMembers
				WHERE id=@vStaffMemberId;

				PRINT 'Generated missing attendance for ' + @vName + ' (' + CAST(@vPayrollNo AS varchar(15)) + ')';
			END
			IF @@TRANCOUNT>0 COMMIT TRAN;

		END
	END TRY
	BEGIN CATCH
		SET @retcode = ERROR_NUMBER();
		IF @@TRANCOUNT>0 ROLLBACK TRAN;		
		PRINT ERROR_MESSAGE();
	END CATCH

	CLOSE new_staff_cursor;

	DEALLOCATE new_staff_cursor;

	RETURN @retcode
END