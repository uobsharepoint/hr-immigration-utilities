USE hr_immigration
GO

IF SCHEMA_ID('Utils2018') IS NULL EXEC('CREATE SCHEMA Utils2018 AUTHORIZATION dbo');
GO

IF OBJECT_ID('Utils2018.getStaffMemberDetails') IS NOT NULL DROP PROCEDURE Utils2018.getStaffMemberDetails;
GO

CREATE PROCEDURE [utils2018].[getStaffMemberDetails](@StaffMemberId int=0, @PayrollNo int = 0, @FirstName nvarchar(100) ='', @LastName nvarchar(100) = '') 
AS
BEGIN
	IF @StaffMemberId<>0 
		SELECT m.id, m.PayrollNo, m.SchoolId, s.OrganisationDescription AS School,c.OrganisationDescription AS College, m.FirstName, m.LastName, m.ImmigrationStatus, m.AppointmentStartDate, m.AppointmentExpectedEndDate, m.AppointmentEndDate, m.AttendanceStartDate, m.AttendanceEndDate, m.isApproved, m.isActive
		FROM dbo.StaffMembers m INNER JOIN dbo.Schools s
		ON m.SchoolId = s.id
		INNER JOIN dbo.Colleges c ON s.CollegeId=c.id
		WHERE m.id = @StaffMemberId
	ELSE IF @PayrollNo<>0
		SELECT m.id, m.PayrollNo, m.SchoolId, s.OrganisationDescription AS School,c.OrganisationDescription AS College, m.FirstName, m.LastName, m.ImmigrationStatus, m.AppointmentStartDate, m.AppointmentExpectedEndDate, m.AppointmentEndDate, m.AttendanceStartDate, m.AttendanceEndDate, m.isApproved, m.isActive
		FROM dbo.StaffMembers m INNER JOIN dbo.Schools s
		ON m.SchoolId = s.id
		INNER JOIN dbo.Colleges c ON s.CollegeId=c.id
		WHERE PayrollNo = @PayrollNo
	ELSE IF @LastName<>'' AND @FirstName<>''
		SELECT m.id, m.PayrollNo, m.SchoolId, s.OrganisationDescription AS School,c.OrganisationDescription AS College, m.FirstName, m.LastName, m.ImmigrationStatus, m.AppointmentStartDate, m.AppointmentExpectedEndDate, m.AppointmentEndDate, m.AttendanceStartDate, m.AttendanceEndDate, m.isApproved, m.isActive
		FROM dbo.StaffMembers m INNER JOIN dbo.Schools s
		ON m.SchoolId = s.id
		INNER JOIN dbo.Colleges c ON s.CollegeId=c.id
		WHERE PayrollNo = @PayrollNo AND FirstName=@FirstName AND LastName=@LastName
	ELSE IF @LastName<>'' AND @FirstName=''
		SELECT m.id, m.PayrollNo, m.SchoolId, s.OrganisationDescription AS School,c.OrganisationDescription AS College, m.FirstName, m.LastName, m.ImmigrationStatus, m.AppointmentStartDate, m.AppointmentExpectedEndDate, m.AppointmentEndDate, m.AttendanceStartDate, m.AttendanceEndDate, m.isApproved, m.isActive
		FROM dbo.StaffMembers m INNER JOIN dbo.Schools s
		ON m.SchoolId = s.id
		INNER JOIN dbo.Colleges c ON s.CollegeId=c.id
		WHERE PayrollNo = @PayrollNo AND  LastName=@LastName
	ELSE IF @LastName='' AND @FirstName<>''
		SELECT m.id, m.PayrollNo, m.SchoolId, s.OrganisationDescription AS School,c.OrganisationDescription AS College, m.FirstName, m.LastName, m.ImmigrationStatus, m.AppointmentStartDate, m.AppointmentExpectedEndDate, m.AppointmentEndDate, m.AttendanceStartDate, m.AttendanceEndDate, m.isApproved, m.isActive
		FROM dbo.StaffMembers m INNER JOIN dbo.Schools s
		ON m.SchoolId = s.id
		INNER JOIN dbo.Colleges c ON s.CollegeId=c.id
		WHERE PayrollNo = @PayrollNo AND FirstName=@FirstName 
	ELSE
		SELECT 'Invalid query'

END
GO

IF OBJECT_ID('utils2018.getAttendanceDetails') IS NOT NULL DROP PROCEDURE utils2018.getAttendanceDetails;
GO


CREATE PROCEDURE utils2018.getAttendanceDetails(@PayrollNo int=0, @StaffMemberId int=0, @StartDate date=NULL, @isActive bit=1)
AS 
BEGIN
	DECLARE @vStartDate date;
	DECLARE @vStaffMemberId int = @StaffMemberId;

	IF @payrollno = 0 AND @staffmemberid = 0
	BEGIN
		PRINT 'Either payroll no or staff member id must be specifed'
		RETURN 50099
	END

	IF @vStaffMemberId=0 AND @PayrollNo <> 0 
		SELECT TOP 1 @vStaffMemberId = id 
		FROM dbo.StaffMembers 
		WHERE PayrollNo=@PayrollNo AND IsActive=@isActive
		ORDER BY AppointmentStartDate DESC;

	IF @StartDate IS NULL 
		SELECT TOP 1 @vStartDate = 
			CASE DATEPART(dw, AppointmentStartDate) 
			WHEN 7 THEN DATEADD(d, 2, AppointmentStartDate) 
			ELSE DATEADD(d, 2 - DATEPART(dw, AppointmentStartDate), AppointmentStartDate)
			END
		FROM StaffMembers WHERE id = @vStaffMemberId;
	ELSE
		SELECT @vStartDate = 
			CASE DATEPART(dw, @StartDate) 
			WHEN 7 THEN DATEADD(d, 2, @StartDate) 
			ELSE DATEADD(d, 2 - DATEPART(dw, @StartDate), @StartDate)
			END


	SELECT 
		sma.Id, 
		sma.StaffMemberId, 
		sma.AttendanceId, 
		att.SchoolId, 
		sch.OrganisationDescription,
		AbsenceTypeId, 
		att.WeekStartDate,
		StartDate,
		EndDate,
		AttendanceType,
		smacd.AttendanceRecords,
		smacd.CompletedRecords
	FROM dbo.StaffMemberAttendances sma
	inner join dbo.Attendances att
	on sma.AttendanceId = att.id
	inner join dbo.schools sch
	on sch.id = att.SchoolId
	LEFT JOIN (SELECT AttendanceId, StaffMemberId, COUNT(*) AttendanceRecords, SUM(CAST(isComplete AS tinyint)) CompletedRecords FROM StaffMemberAttendanceCompletionDatas GROUP BY AttendanceId, StaffMemberId) smacd
	ON sma.StaffMemberId=smacd.StaffMemberId AND sma.AttendanceId=smacd.AttendanceId
	where SMA.StaffMemberId = @vStaffMemberId
	and att.WeekStartDate>= @vStartDate
	ORDER BY att.WeekStartDate;
END
GO

IF OBJECT_ID('Utils2018.CreateMissingStaffAttendances') IS NOT NULL DROP PROCEDURE Utils2018.CreateMissingStaffAttendances;
GO

CREATE PROCEDURE utils2018.CreateMissingStaffAttendances(@PayrollNo int=0, @StaffMemberId int=0, @SchoolId int=0, @FromDate date=NULL, @AttendanceType tinyint=4, @Complete bit=0)
AS
BEGIN
	DECLARE @vStaffMemberId int = @StaffMemberId, @vSchoolId int, @vAttendanceStart date, @vAttendanceEnd date;
	DECLARE @vFromDate date;
	DECLARE @dow tinyint = 1;
	DECLARE @retcode int = 0;

	IF @StaffMemberId=0 AND @PayrollNo<>0
	BEGIN
		SELECT TOP 1 @vStaffMemberId=id
		FROM dbo.StaffMembers
		WHERE PayrollNo=@PayrollNo
		ORDER BY AttendanceStartDate DESC;
	END

	IF @vStaffMemberId<>0
	BEGIN
		SELECT TOP 1 @vSchoolId=SchoolId, @vAttendanceStart=AttendanceStartDate, @vAttendanceEnd=ISNULL(ISNULL(AttendanceEndDate, AppointmentEndDate), '2099-12-31')
		FROM dbo.StaffMembers
		WHERE id=@vStaffMemberId
	END

	IF @SchoolId<>0 SET @vSchoolId = @SchoolId;

	IF @vStaffMemberId = 0 OR @vSchoolId=0 
	BEGIN
		PRINT 'Could not find either a valid staff member id or valid school id'
		RETURN 50099
	END

	SELECT @vFromDate = 
		CASE datepart(dw, ISNULL(@FromDate, @vAttendanceStart)) 
		WHEN 7 THEN DATEADD(dd, 2, ISNULL(@FromDate, @vAttendanceStart)) -- following Monday
		ELSE DATEADD(dd, 2 - datepart(dw, ISNULL(@FromDate, @vAttendanceStart)), ISNULL(@FromDate, @vAttendanceStart)) -- nearest Monday
		END;

	PRINT 'Fixing attendance records for ' + CAST(@vStaffMemberId AS varchar(10)) + ' IN ' + CAST(@vSchoolId AS varchar(10)) + ' FROM ' + CONVERT(varchar(15), @FromDate, 103 );

	BEGIN TRAN;

	BEGIN TRY
		INSERT INTO dbo.StaffMemberAttendances
		(StaffMemberId, AttendanceId, AbsenceTypeId, ContactDetails, IsLeaving, LeavingReason, NewEmployerDetails, StartDate, EndDate, AttendanceType)
		SELECT @vStaffMemberId, a.id, NULL, NULL, 0, NULL, NULL, NULL, NULL, @AttendanceType
		FROM dbo.Attendances a
		LEFT JOIN (
			SELECT id, AttendanceId
			FROM StaffMemberAttendances
			WHERE StaffMemberId=@vStaffMemberId
		) s
		ON a.id=s.AttendanceId
		WHERE a.schoolid = @vSchoolId
		AND a.WeekStartDate BETWEEN @vFromDate AND @vAttendanceEnd
		AND s.id IS NULL;

		WHILE @dow<=5
		BEGIN
			INSERT INTO dbo.StaffMemberAttendanceCompletionDatas
			([DayOfWeek], StaffMemberId, AttendanceId, IsComplete)
			SELECT 
				@dow, 
				@vStaffMemberId, 
				sma.AttendanceId, 
				CASE 
				WHEN DATEADD(d, @dow-1, att.WeekStartDate)<@vAttendanceStart THEN 1
				ELSE @Complete
				END
			FROM dbo.StaffMemberAttendances sma
			INNER JOIN  dbo.Attendances att
			ON sma.AttendanceId = att.id
			WHERE sma.StaffMemberId = @vStaffMemberId
			AND att.WeekStartDate >= @vFromDate
			AND NOT EXISTS ( 
				SELECT 0 FROM dbo.StaffMemberAttendanceCompletionDatas smacd
				WHERE smacd.StaffMemberId=@vStaffMemberId AND smacd.AttendanceId=sma.AttendanceId AND smacd.[DayOfWeek]=@dow
			)

			SET @dow = @dow + 1;
		END
		
		IF @@TRANCOUNT>0 COMMIT TRAN;

		PRINT CAST(@@ROWCOUNT AS varchar(15)) + ' attendance records created';

		SELECT sma.id, sma.StaffMemberId, sma.AttendanceId, a.SchoolId, a.WeekStartDate, smacd.[DayOfWeek], sma.AttendanceType, smacd.IsComplete
		FROM dbo.StaffMemberAttendances sma 
		INNER JOIN dbo.Attendances a 
		ON a.id = sma.AttendanceId
		INNER JOIN dbo.StaffMemberAttendanceCompletionDatas smacd
		ON smacd.AttendanceId=sma.AttendanceId AND sma.StaffMemberId=smacd.StaffMemberId
		WHERE sma.StaffMemberId = @vStaffMemberId AND a.WeekStartDate>=@vFromDate
		ORDER BY a.WeekStartDate, smacd.[DayOfWeek] 
	END TRY
	BEGIN CATCH
		PRINT ERROR_MESSAGE();
		IF @@TRANCOUNT>0 ROLLBACK TRAN;
		SET @retcode = ERROR_NUMBER();
	END CATCH

	RETURN @retcode;
END
GO


IF OBJECT_ID('Utils2018.FixStaffSchoolChange') IS NOT NULL DROP PROCEDURE Utils2018.FixStaffSchoolChange;
GO

CREATE PROCEDURE utils2018.FixStaffSchoolChange(@PayrollNo int=0, @StaffMemberId int=0, @OldSchoolId int, @NewSchoolId int, @OldAppointmentStart date, @NewAppointmentStart date)
AS
BEGIN
	DECLARE @OldStaffMemberId int, @NewStaffMemberId int;
	DECLARE @OldAttendanceStart date, @NewAttendanceStart date;
	DECLARE @AttendancesUpdated int;
	DECLARE @retcode int = 0;

	IF @PayrollNo = 0 AND @StaffMemberId = 0 
	BEGIN
		PRINT 'Either the payroll number of staff member id must be specified'
		RETURN 50099
	END

	SET @OldStaffMemberId = @StaffMemberId;	

	IF @StaffMemberId = 0 AND @PayrollNo <> 0
	BEGIN
		SELECT TOP 1 @OldStaffMemberId=id
		FROM dbo.StaffMembers
		WHERE PayrollNo = @PayrollNo
		AND isActive = 1;
	END 

	SET @OldAttendanceStart = dateadd(dd, 2 - datepart(dw, @OldAppointmentStart), @OldAppointmentStart);
	SET @NewAttendanceStart = dateadd(dd, 2 - datepart(dw, @NewAppointmentStart), @NewAppointmentStart);

	BEGIN TRANSACTION;

	BEGIN TRY
		INSERT INTO dbo.StaffMembers
				(AmaResponsibleForAttendanceMonitoring
				,ApplicationExtensionComments
				,ApplicationExtensionDate
				,SchoolId
				,AppointmentExpectedEndDate
				,AppointmentStartDate
				,Category
				,CosEndDate
				,CosStartDate
				,DetailsLastChecked
				,FirstName
				,Fte
				,Grade
				,[Group]
				,HasAttendanceMonitoredToDate
				,ImmigrationStatus
				,IsRequiredToMonitorAttendance
				,JobTitle
				,LastName
				,LtrEndDate
				,LtrLastChecked
				,LtrStartDate
				,NationalInsuranceNumber
				,PayrollNo
				,Salary
				,VisaNo
				,Discriminator
				,IsActive
				,AppointmentEndDate
				,Allowances
				,CountryOfResidence
				,CosNumber
				,CosEndReasons
				,DateOfBirth
				,Funds
				,Gender
				,HomeAddress
				,IsCertifyingMaintenance
				,IsJobNQFLevel6
				,MultipleEntryVisa
				,NationalIdNumber
				,Nationality
				,PassportIssueDate
				,PersonalDetails
				,PlaceOfBirth
				,WeeklyHours
				,HomePostcode
				,IsApproved
				,EmailAddress
				,AppointmentDueToFinish
				,AttendanceStartDate
				,AttendanceEndDate
				,AttendanceDateChangeComments)
		SELECT 
			AmaResponsibleForAttendanceMonitoring
			,ApplicationExtensionComments
			,ApplicationExtensionDate
			,@NewSchoolId
			,AppointmentExpectedEndDate
			,@NewAppointmentStart 	-- AppointmentStartDate
			,Category
			,CosEndDate
			,CosStartDate
			,DetailsLastChecked
			,FirstName
			,Fte
			,Grade
			,[Group]
			,HasAttendanceMonitoredToDate
			,ImmigrationStatus
			,IsRequiredToMonitorAttendance
			,JobTitle
			,LastName
			,LtrEndDate
			,LtrLastChecked
			,LtrStartDate
			,NationalInsuranceNumber
			,PayrollNo
			,Salary
			,VisaNo
			,Discriminator
			,1			-- isActive
			,NULL		-- AppointmentEndDate
			,Allowances
			,CountryOfResidence
			,CosNumber
			,CosEndReasons
			,DateOfBirth
			,Funds
			,Gender
			,HomeAddress
			,IsCertifyingMaintenance
			,IsJobNQFLevel6
			,MultipleEntryVisa
			,NationalIdNumber
			,Nationality
			,PassportIssueDate
			,PersonalDetails
			,PlaceOfBirth
			,WeeklyHours
			,HomePostcode
			,IsApproved
			,EmailAddress
			,AppointmentDueToFinish
			,@NewAttendanceStart 
			,AttendanceEndDate
			,AttendanceDateChangeComments
		FROM dbo.StaffMembers
		WHERE id = @OldStaffMemberId;

		SELECT @NewStaffMemberId = SCOPE_IDENTITY();

		UPDATE dbo.StaffMembers
		SET
			PayrollNo = PayrollNo * -1,
			SchoolId = @OldSchoolId,
			AppointmentStartDate = @OldAppointmentStart,
			AppointmentEndDate = DATEADD(d, -1, @NewAppointmentStart),
			AttendanceEndDate = DATEADD(d, -1, @NewAppointmentStart),
			IsActive = 0
		WHERE id = @OldStaffMemberId;

		UPDATE StaffMemberAttendances
		SET StaffMemberId = @NewStaffMemberId
		WHERE StaffMemberId = @OldStaffMemberId
		AND AttendanceId IN (
			SELECT id
			FROM Attendances
			WHERE SchoolId=@NewSchoolId
			AND WeekStartDate >= @NewAttendanceStart
		);

		SELECT @AttendancesUpdated = @@ROWCOUNT;

		IF @@TRANCOUNT>0 COMMIT TRANSACTION;
		PRINT 'Completed - new staff member id = '+ CAST(@NewStaffMemberId AS varchar(10)) + CHAR(10);
		PRINT CAST(@AttendancesUpdated AS varchar(10)) + ' attendance records switched to new staff member record';
	END TRY
	BEGIN CATCH
		PRINT ERROR_MESSAGE();
		SET @retcode = ERROR_NUMBER();
		IF @@TRANCOUNT>0 ROLLBACK TRANSACTION;
	END CATCH;

	RETURN @retcode;
END
GO

IF OBJECT_ID('Utils2018.FixMissingCompletionData') IS NOT NULL DROP PROCEDURE Utils2018.FixMissingCompletionData;
GO

CREATE PROCEDURE utils2018.FixMissingCompletionData(@cutoff date) 
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE missing_completion_data CURSOR FOR
	SELECT sma.StaffMemberId, sma.AttendanceId, stf.PayrollNo, stf.FirstName, stf.LastName, att.WeekStartDate
	FROM StaffMemberAttendances sma
	INNER JOIN Attendances att
	ON att.id=sma.AttendanceId
	INNER JOIN StaffMembers stf
	ON stf.id=sma.StaffMemberId
	WHERE NOT EXISTS (
		SELECT 0 FROM StaffMemberAttendanceCompletionDatas smacd
		WHERE smacd.AttendanceId=sma.AttendanceId AND smacd.StaffMemberId=sma.StaffMemberId
	)
	AND att.WeekStartDate>= @cutoff;

	DECLARE @vStaffMemberId int, @vAttendanceId int, @vPayrollNo int, @vFirstName nvarchar(100), @vLastName nvarchar(100), @vWeekStartDate date;

	OPEN missing_completion_data;

	WHILE (1=1)
	BEGIN
		FETCH NEXT FROM missing_completion_data
		INTO @vStaffMemberId, @vAttendanceId, @vPayrollNo, @vFirstName, @vLastName, @vWeekStartDate;

		IF @@FETCH_STATUS <> 0 BREAK;

		BEGIN TRY
			PRINT 'Generating missing attendances for ' + @vFirstName + ' ' + @vLastName + ' (' + CAST(@vPayrollNo AS varchar(15)) + ') for w/c ' + CONVERT(varchar(10), @vWeekStartDate, 103 );

			INSERT INTO StaffMemberAttendanceCompletionDatas
			([DayOfWeek], StaffMemberId, AttendanceId, IsComplete)
			VALUES
			(1, @vStaffMemberId, @vAttendanceId, 0),
			(2, @vStaffMemberId, @vAttendanceId, 0),
			(3, @vStaffMemberId, @vAttendanceId, 0),
			(4, @vStaffMemberId, @vAttendanceId, 0),
			(5, @vStaffMemberId, @vAttendanceId, 0);

			PRINT 'Successfully generated missing attendances';		
		END TRY
		BEGIN CATCH
			PRINT ERROR_MESSAGE();
		END CATCH
	END
	CLOSE missing_completion_data;

	DEALLOCATE missing_completion_data;
END

GO

IF OBJECT_ID('Utils2018.GetStaffWithMissingAttendances') IS NOT NULL DROP PROCEDURE Utils2018.GetStaffWithMissingAttendances;
GO

CREATE PROCEDURE utils2018.GetStaffWithMissingAttendances(@FromDate date, @IncludeInactive bit=0)
AS 
BEGIN
	DECLARE @staging TABLE (id int NOT null, ImmigrationStatus varchar(10) NOT NULL, WeekStartDate date NOT NULL);

	INSERT INTO @staging
	SELECT 
		stf.id, 
		CASE stf.ImmigrationStatus
		WHEN 1 THEN 'Tier 2'
		WHEN 2 THEN 'Tier 5'
		ELSE 'unknown'
		END,
		att.WeekStartDate
	FROM StaffMembers stf
	INNER JOIN Attendances att
	ON stf.SchoolId=att.SchoolId AND att.WeekStartDate BETWEEN stf.AppointmentStartDate AND ISNULL(stf.AppointmentEndDate, '2099-12-31')
	LEFT JOIN StaffMemberAttendances sma
	ON sma.StaffMemberId=stf.id AND sma.AttendanceId=att.id
	WHERE stf.AppointmentStartDate>=@FromDate 
	AND stf.ImmigrationStatus in (1,2) -- tier 2 and tier 5
	AND sma.id IS NULL;

	IF @IncludeInactive = 0
	BEGIN
		SELECT 
			m1.id as StaffMemberId, 
			PayrollNo, 
			stf1.SchoolId, 
			sch.OrganisationDescription AS School, 
			FirstName, 
			LastName, 
			AppointmentStartDate, 
			m1.ImmigrationStatus,
			MIN(m1.WeekStartDate) AS FirstWeek, 
			MAX(m1.WeekStartDate)  AS LastWeek
		FROM @staging m1
		INNER JOIN StaffMembers stf1
		ON m1.id=stf1.id
		INNER JOIN Schools sch
		ON sch.id=stf1.SchoolId	
		WHERE stf1.isActive=1 -- AND  sch.IsActive = 1
		GROUP BY m1.id, PayrollNo, stf1.SchoolId, sch.OrganisationDescription, FirstName, LastName, AppointmentStartDate, m1.ImmigrationStatus
		ORDER BY sch.OrganisationDescription, stf1.LastName;
	END
	ELSE
	BEGIN
		SELECT 
			m1.id as StaffMemberId, 
			PayrollNo, 
			stf1.SchoolId, 
			sch.OrganisationDescription AS School, 
			FirstName, 
			LastName, 
			AppointmentStartDate, 
			m1.ImmigrationStatus,
			stf1.IsActive,
			MIN(m1.WeekStartDate) AS FirstWeek, 
			MAX(m1.WeekStartDate)  AS LastWeek
		FROM @staging m1
		INNER JOIN StaffMembers stf1
		ON m1.id=stf1.id
		INNER JOIN Schools sch
		ON sch.id=stf1.SchoolId	
		GROUP BY m1.id, PayrollNo, stf1.SchoolId, sch.OrganisationDescription, FirstName, LastName, AppointmentStartDate, m1.ImmigrationStatus, stf1.IsActive
		ORDER BY sch.OrganisationDescription, stf1.LastName;
	END
END
GO



IF OBJECT_ID('utils2018.GetIncompleteAttendances') IS NOT NULL DROP PROCEDURE utils2018.GetIncompleteAttendances
GO

CREATE PROCEDURE utils2018.GetIncompleteAttendances(@PayrollNo int=0, @StaffMemberId int=0)
AS
BEGIN
	DECLARE @vStaffMemberId int = @StaffMemberId;

	IF @PayrollNo=0 AND @StaffMemberId=0
	BEGIN
		PRINT 'Either the payroll # or staff member id must be specifed';
		RETURN 50099;
	END

	IF @vStaffMemberId=0
	BEGIN
		SELECT TOP 1 @vStaffMemberId=id
		FROM StaffMembers
		WHERE PayrollNo=@PayrollNo
		ORDER BY AppointmentStartDate desc;
	END

	SELECT 
		smacd.AttendanceId, 
		att.WeekStartDate,
		sum(CAST(smacd.isComplete AS tinyint)) AS Completed
	FROM StaffMemberAttendanceCompletionDatas smacd
	inner join attendances att
	on att.id=smacd.AttendanceId
	inner join StaffMemberAttendances sma
	ON sma.AttendanceId=smacd.AttendanceId
	AND sma.StaffMemberId=smacd.StaffMemberId
	where smacd.StaffMemberId=@vStaffMemberId AND sma.AttendanceType=4
	GROUP BY smacd.AttendanceId, att.WeekStartDate
	HAVING sum(CAST(smacd.isComplete AS tinyint))=0;
END
GO

