USE HR_IMMIGRATION;
GO

IF SCHEMA_ID('New_Core') IS NULL EXEC('CREATE SCHEMA New_Core AUTHORIZATION dbo');
GO

IF OBJECT_ID('New_Core.GetStaffMembersThatHaveOutstandingPersonalDetailsChecks') IS NOT NULL DROP PROCEDURE New_Core.GetStaffMembersThatHaveOutstandingPersonalDetailsChecks;
GO

CREATE PROCEDURE New_Core.GetStaffMembersThatHaveOutstandingPersonalDetailsChecks
AS
BEGIN
    DECLARE @threshold_start date = CAST('2017-12-01' AS date);
    DECLARE @threshold_end date = CAST('2018-03-01' AS date);

    SELECT 
        ST.id,
		st.FirstName,
		st.LastName,
		st.EmailAddress,
		ST.AppointmentEndDate, 
		st.AppointmentStartDate,
		st.DetailsLastChecked,
		st.ImmigrationStatus,
		st.LtrEndDate,
		st.LtrStartDate,
        C.OrganisationCode AS College
    FROM dbo.StaffMembers ST 
    INNER JOIN dbo.Schools SC ON ST.SchoolId = SC.Id 
    INNER JOIN dbo.Colleges C ON SC.CollegeId = C.ID
    WHERE ST.IsActive = 1
    AND ST.IsApproved = 1
    AND (ST.ImmigrationStatus = 1   -- tier 2
        OR ST.ImmigrationStatus = 2 -- tier 5 ?
    )
    AND (ST.AppointmentStartDate IS NULL 
        OR (ST.DetailsLastChecked IS NULL AND ST.AppointmentStartDate BETWEEN @threshold_start AND @threshold_end )
        OR (ST.DetailsLastChecked IS NOT NULL AND ST.DetailsLastChecked BETWEEN @threshold_start AND @threshold_end  )
    )
    AND (ST.AppointmentEndDate IS NULL OR ST.AppointmentEndDate>=GETDATE());

END
GO

IF OBJECT_ID('New_Core.GetStaffMembersThatHaveLtrCheckDue') IS NOT NULL DROP PROCEDURE New_Core.GetStaffMembersThatHaveLtrCheckDue;
GO

CREATE PROCEDURE New_Core.GetStaffMembersThatHaveLtrCheckDue
AS
BEGIN
    DECLARE @refDate date = CAST(GETDATE() AS date);
    DECLARE @threshold_start date = CAST('2018-12-01' AS date);
    DECLARE @threshold_end date = CAST('2019-03-01' AS date);

    SELECT 
        st.id,
        
		st.FirstName,
		st.LastName,
		st.EmailAddress,
		ST.AppointmentEndDate, 
		st.AppointmentStartDate,
		st.DetailsLastChecked,
		st.ImmigrationStatus,
		st.LtrEndDate,
		st.LtrStartDate,
        SC.OrganisationCode AS School
    FROM dbo.StaffMembers ST 
    INNER JOIN dbo.Schools SC ON ST.SchoolId = SC.Id 
    WHERE ST.IsActive = 1
    AND ST.IsApproved = 1
    AND ST.AppointmentStartDate < GETDATE()
    AND ST.ApplicationExtensionDate IS NULL AND ST.LtrEndDate IS NOT NULL
    AND ST.LtrEndDate BETWEEN @threshold_start AND @threshold_end
    AND (ST.AppointmentEndDate IS NULL OR ST.AppointmentEndDate>=GETDATE());
END

GO

IF OBJECT_ID('New_Core.IAWeeklyAttendanceMoniitoring') IS NOT NULL DROP PROCEDURE New_Core.IAWeeklyAttendanceMoniitoring;
GO


CREATE PROCEDURE New_Core.IAWeeklyAttendanceMoniitoring (@CutoffFrom tinyint=56, @CutoffTo tinyint=21)
AS
BEGIN
	IF @CutoffFrom<@CutoffTo 
	BEGIN
		PRINT 'Invalid report interval';
		RETURN 50099;
	END

	BEGIN
		WITH Attendance_Completion_Summary (StaffMemberId, AttendanceId, Completed)
		AS (
			select staffmemberid, attendanceid, sum(cast(iscomplete as tinyint)) completed
			from StaffMemberAttendanceCompletionDatas
			group by staffmemberid, attendanceid
		),
		Staff_Member_Attendances (StaffMemberId, AttendanceId, SchoolId, WeekStartDate)
		AS (
			SELECT s.Id, a.Id, s.SchoolId, a.WeekStartDate
			FROM StaffMembers s
			INNER JOIN Attendances a
			ON s.SchoolId=a.SchoolId
			WHERE a.WeekStartDate>=s.AttendanceStartDate
		)

		select
			s.PayrollNo, 
			s.FirstName,
			s.LastName,
			s.FirstName+' '+s.lastname AS StaffMember, 
			s.SchoolId, 
			sch.OrganisationDescription AS SchoolName, 
			y.FirstName+' '+y.LastName AS ContactName, 
			y.UserName AS ContactUsername, 
			sch.HeadOfSchools, 
			sma.WeekStartDate, 
			acs.completed
		from StaffMembers s
		inner join Schools sch
		on sch.id=s.SchoolId 
		INNER JOIN Staff_Member_Attendances sma
		ON sma.StaffMemberId=s.id
		LEFT JOIN Attendance_Completion_Summary acs
		on sma.StaffMemberId=acs.StaffMemberId AND sma.AttendanceId=acs.AttendanceId
		inner join (
			SELECT au.FirstName, au.lastname, au.UserName, auo.SchoolId FROM 
			ApplicationUsers AU
			INNER JOIN ApplicationUserOrganisations auo
			on au.Id=auo.ApplicationUserId
			WHERE AU.isActive=1
			and AU.Role=4 -- AMA
		) y
		on y.SchoolId=sch.Id		
		where s.ImmigrationStatus in (1,2) and s.IsActive = 1 AND s.IsApproved=1
		and sma.WeekStartDate BETWEEN dbo.fnNearestMonday(dateadd(d, -1*@CutoffFrom, GETDATE())) AND dbo.fnNearestMonday(dateadd(d, -1*@CutoffTo, GETDATE()))
		AND ISNULL(s.AttendanceEndDate, '2099-12-31')>=sma.WeekStartDate 
		and isnull(acs.completed,0)<5
		order by sch.OrganisationDescription, s.LastName, sma.WeekStartDate;
	END

	RETURN 0;
END

GO