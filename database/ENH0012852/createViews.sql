USE HR_Immigration
GO

IF OBJECT_ID('dbo.vwActiveStaff') IS NOT NULL DROP VIEW dbo.vwActiveStaff;
GO

CREATE VIEW dbo.vwActiveStaff AS
SELECT 
	Id, 
	SchoolId, 
	PayrollNo,
	AppointmentStartDate, 
	AppointmentExpectedEndDate,
	AppointmentEndDate, 
	AttendanceStartDate, 
	AttendanceEndDate,
	ImmigrationStatus 
FROM dbo.StaffMembers 
WHERE ImmigrationStatus IN (1,2) AND IsActive = 1 AND IsApproved = 1;
GO

IF OBJECT_ID('dbo.vwStaffAttendanceWeeks') IS NOT NULL DROP VIEW dbo.vwStaffAttendanceWeeks;
GO

CREATE VIEW dbo.vwStaffAttendanceWeeks AS
SELECT STAFF.*, ATT.AttendanceId, ATT.WeekStartDate, ATT.WeekEndDate
FROM (
SELECT 
	Id AS StaffMemberId, 
	SchoolId, 
	AppointmentStartDate, 
	AttendanceStartDate, 
	AppointmentEndDate,
	AppointmentExpectedEndDate ,
	AttendanceEndDate
FROM dbo.StaffMembers 
WHERE ImmigrationStatus IN (1,2) AND IsActive = 1 AND IsApproved = 1) AS STAFF
INNER JOIN
(SELECT Id AS AttendanceId, SchoolId, WeekStartDate, DATEADD(d, 4,WeekStartDate) AS WeekEndDate from dbo.Attendances)
AS ATT
ON STAFF.SchoolId = ATT.SchoolId
WHERE ISNULL(STAFF.AttendanceStartDate, STAFF.AppointmentStartDate)<=ATT.WeekEndDate
AND COALESCE(STAFF.AttendanceEndDate, STAFF.AppointmentEndDate, STAFF.AppointmentExpectedEndDate, '2099-12-31')>=ATT.WeekStartDate;

GO

IF OBJECT_ID('dbo.vwStaffWithMissingAttendance') IS NOT NULL DROP VIEW dbo.vwStaffWithMissingAttendance;
GO

CREATE VIEW dbo.vwStaffWithMissingAttendance AS
SELECT saw.StaffMemberId, saw.AttendanceId
FROM dbo.vwStaffAttendanceWeeks saw
LEFT JOIN dbo.StaffMemberAttendances sma
on saw.StaffMemberId=sma.StaffMemberId 
AND saw.AttendanceId=sma.AttendanceId
WHERE sma.Id IS NULL;

GO

IF OBJECT_ID('dbo.vwStaffWithMissingAttData') IS NOT NULL DROP VIEW dbo.vwStaffWithMissingAttData;
GO

CREATE VIEW dbo.vwStaffWithMissingAttData AS
SELECT sawu.[DayOfWeek], sawu.StaffMemberId, sawu.AttendanceId
FROM (
	SELECT 1 as [DayOfWeek], StaffMemberId, AttendanceId
	FROM vwStaffAttendanceWeeks
	UNION
	SELECT 2, StaffMemberId, AttendanceId
	FROM vwStaffAttendanceWeeks
	UNION
	SELECT 3, StaffMemberId, AttendanceId
	FROM vwStaffAttendanceWeeks
	UNION
	SELECT 4, StaffMemberId, AttendanceId
	FROM vwStaffAttendanceWeeks
	UNION
	SELECT 5, StaffMemberId, AttendanceId
	FROM dbo.vwStaffAttendanceWeeks
) sawu
LEFT JOIN dbo.StaffMemberAttendanceCompletionDatas sma
on sawu.[DayOfWeek]=sma.[DayOfWeek] 
AND sawu.StaffMemberId=sma.StaffMemberId
AND sawu.AttendanceId=sma.AttendanceId
where sma.AttendanceId IS NULL ;

GO
