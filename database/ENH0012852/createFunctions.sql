USE HR_Immigration
GO

IF OBJECT_ID('dbo.getImmigrationStatus') IS NOT NULL DROP FUNCTION dbo.getImmigrationStatus;
GO

CREATE FUNCTION dbo.getImmigrationStatus(@im int)
RETURNS int
AS
BEGIN
	DECLARE @ImmigrationStatus int = 0;

	SELECT @ImmigrationStatus = CASE @im
		WHEN 3 THEN	4	-- UK Permit
		WHEN 5 THEN	1	-- Tier 2
		WHEN 6 THEN 8	-- Tier 1
		WHEN 7 THEN 16	-- Tier 4
		WHEN 8 THEN 32	-- Dependent
		WHEN 9 THEN 64	-- Croation
		WHEN 10 THEN 128	-- Other
		ELSE 0
	END 

	RETURN @ImmigrationStatus;
END
GO

GRANT EXECUTE ON dbo.getImmigrationStatus TO public;
GO

