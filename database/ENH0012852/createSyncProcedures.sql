USE HR_Immigration
GO

IF OBJECT_ID('dbo.loadStagingTable') IS NOT NULL DROP PROCEDURE dbo.loadStagingTable;
GO

CREATE PROCEDURE dbo.loadStagingTable(@feed varchar(20), @xml xml, @msg nvarchar(4000) out, @records int out)
AS
BEGIN
	DECLARE @retcode int = 0;
	SET @msg = ISNULL(@msg, '');
	IF @msg<>'' SET @msg = @msg + CHAR(13) + CHAR(10);

	BEGIN TRY
		SET @msg = @msg + @feed + CHAR(13) + CHAR(10);

		IF LOWER(@feed) = 'colleges'
		BEGIN

			DELETE FROM Staging.Colleges;

			INSERT INTO Staging.Colleges (CollegeOrgCode, CollegeOrg)
			SELECT 
				RTRIM(T.C.value('(CollegeOrgCode/text())[1]', 'nvarchar(20)')) ,
				RTRIM(T.C.value('(CollegeOrg/text())[1]', 'nvarchar(100)')) 
			FROM @xml.nodes('Colleges/college') T(C);		

			SET @records = @@ROWCOUNT;

		END

		IF LOWER(@feed) = 'schools'
		BEGIN
			DELETE FROM Staging.Schools;

			INSERT INTO Staging.Schools (CollegeOrgCode, SchoolOrgCode, SchoolOrg, HeadOfSchool)
			SELECT 
				RTRIM(T.C.value('(CollegeOrgCode/text())[1]', 'nvarchar(20)')),
				RTRIM(T.C.value('(SchoolOrgCode/text())[1]', 'nvarchar(20)')) ,
				RTRIM(T.C.value('(SchoolOrg/text())[1]', 'nvarchar(100)')) ,
				RTRIM(T.C.value('((Heads/Head)[1]/Forename/text())[1]', 'nvarchar(100)')) + ' ' + RTRIM(T.C.value('((Heads/Head)[1]/Surname/text())[1]', 'nvarchar(100)')) + ' <' + UPPER(RTRIM(T.C.value('((Heads/Head)[1]/EmailAddress/text())[1]', 'nvarchar(100)'))) + '>'
			FROM @xml.nodes('schools/school') T(C)
			SET @records = @@ROWCOUNT;
		END

		IF LOWER(@feed) = 'staff'
		BEGIN
			DELETE FROM Staging.StaffMembers;

			INSERT INTO Staging.StaffMembers (
				ImmigrationStatus, 
				ImmigrationStatusDesc, 
				PersonCode, 
				Forename, 
				Surname, 
				NiNumber, 
				ExpectedEndDate, 
				EndDate, 
				StartDate, 
				JobTitle, 
				AppointedOrgCode, 
				AppointedOrg, 
				SchoolOrganisationCode, 
				SchoolOrganisationDesc, 
				College, 
				Salary, 
				FTE, 
				VisaNo, 
				VisaStart, 
				VisaEnd, 
				StaffCategory, 
				StaffGroup, 
				StaffGrade, 
				EmailAddress
			)
			SELECT 
				T.C.value('(ImmigrationStatus/text())[1]', 'int'),
				RTRIM(T.C.value('(ImmigrationStatusDesc/text())[1]', 'nvarchar(20)')),
				T.C.value('(PersonCode/text())[1]', 'int'),
				RTRIM(T.C.value('(Forename/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(Surname/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(NiNumber/text())[1]', 'nvarchar(100)')),
				T.C.value('(ExpectedEndDate/text())[1]', 'char(10)'),
				T.C.value('(EndDate/text())[1]', 'char(10)'),
				T.C.value('(StartDate/text())[1]', 'char(10)'),
				RTRIM(T.C.value('(JobTitle/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(AppointedOrgCode/text())[1]', 'nvarchar(20)')),
				RTRIM(T.C.value('(AppointedOrg/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(SchoolOrganisationCode/text())[1]', 'nvarchar(20)')),
				RTRIM(T.C.value('(SchoolOrganisationDesc/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(College/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(Salary/text())[1]', 'nvarchar(100)')),
				RTRIM(T.C.value('(FTE/text())[1]', 'nvarchar(15)')),
				RTRIM(T.C.value('(VisaNo/text())[1]', 'nvarchar(20)')),
				T.C.value('(VisaStart/text())[1]', 'char(10)'),
				T.C.value('(VisaEnd/text())[1]', 'char(10)'),
				RTRIM(T.C.value('(StaffCategory/text())[1]', 'nvarchar(10)')),
				RTRIM(T.C.value('(StaffGroup/text())[1]', 'nvarchar(10)')),
				RTRIM(T.C.value('(StaffGrade/text())[1]', 'nvarchar(10)')),
				RTRIM(T.C.value('(EmailAddress/text())[1]', 'nvarchar(100)'))
			FROM @xml.nodes('Immigration/staff') T(C);
			SET @records = @@ROWCOUNT;
		END

		SET @msg = @msg + CAST(@records AS varchar(6)) + ' rows inserted into ' + @feed + ' staging table'+ CHAR(13) + CHAR(10);

	END TRY
	BEGIN CATCH
		SET @retcode = ERROR_NUMBER();
		SET @msg = @msg + ERROR_MESSAGE()
	END CATCH

	RETURN @retcode

END
GO

IF OBJECT_ID('dbo.loadStagingWrapper') IS NOT NULL DROP PROCEDURE dbo.loadStagingWrapper;
GO

CREATE PROCEDURE dbo.loadStagingWrapper(@feed varchar(20), @xml xml)
AS 
BEGIN
	DECLARE @retcode int = 0, @vMsg nvarchar(4000)=N'', @vRecords int = 0;

	EXEC @retcode = dbo.loadStagingTable @feed=@feed, @xml=@xml, @msg = @vMsg out, @records=@vRecords out
	SELECT @retcode AS [ReturnCode], @vRecords AS [RecordCount], @vMsg as [Message]
END
GO


IF OBJECT_ID('dbo.syncCollegesFromStaging') IS NOT NULL DROP PROCEDURE dbo.syncCollegesFromStaging
GO

CREATE PROCEDURE dbo.syncCollegesFromStaging(@msg nvarchar(4000) out, @records int out)
AS
BEGIN
	DECLARE @results TABLE (
		[Status] varchar(15),
		CollegeId int,
		College nvarchar(20),
		isActive bit
	);
	DECLARE @retcode int=0;
	DECLARE @maxid int;

	SET @msg = ISNULL(@msg, '');
	SET @msg = @msg + 'Starting Colleges Sync' + CHAR(13) + CHAR(10);

	SET NOCOUNT ON
	SELECT @maxid = MAX(id) FROM dbo.Colleges;
	SET @maxid = ISNULL(@maxid, 0);

	BEGIN TRY
		MERGE dbo.Colleges AS T
		USING Staging.Colleges AS s
		ON t.OrganisationCode = s.CollegeOrgCode
		WHEN NOT MATCHED BY TARGET THEN
		INSERT (id, OrganisationCode, OrganisationDescription, isActive)
		VALUES(S.fakeid+@maxid, S.CollegeOrgCode, S.CollegeOrg, 1)
		WHEN MATCHED AND T.OrganisationDescription<>S.CollegeOrg THEN
		UPDATE SET OrganisationDescription = S.CollegeOrg
		WHEN NOT MATCHED BY SOURCE AND T.isActive=1 THEN
		UPDATE SET isActive = 0
		OUTPUT 
			CASE
			WHEN $action='UPDATE' AND inserted.isActive=0 THEN 'MADE INACTIVE'
			WHEN $action='INSERT' THEN 'ADDED'
			ELSE $action
			END,
			inserted.id,
			inserted.OrganisationCode,
			inserted.isActive
		INTO @results;

		SELECT @records = (SELECT COUNT(*) FROM @results);

		SET @msg = @msg + CAST(@records AS varchar(6)) + ' records affected'+ CHAR(13) + CHAR(10);
	END TRY
	BEGIN CATCH
		SET @retcode = ERROR_NUMBER();
		SET @msg = @msg + ERROR_MESSAGE()
	END CATCH

	SELECT * FROM @results;

	RETURN @retcode;
END
GO

IF OBJECT_ID('dbo.syncSchoolsFromStaging') IS NOT NULL DROP PROCEDURE dbo.syncSchoolsFromStaging
GO

CREATE PROCEDURE dbo.syncSchoolsFromStaging(@msg nvarchar(4000) out, @records int out)
AS
BEGIN
	DECLARE @results TABLE (
		[Status] varchar(15),
		SchoolId int,
		School nvarchar(20),
		isActive bit
	);
	DECLARE @retcode int=0, @maxid int;

	SET NOCOUNT ON

	SELECT @maxid = MAX(id) FROM dbo.Schools;
	SET @maxid = ISNULL(@maxid, 0);

	SET @msg = ISNULL(@msg, '');
	SET @msg = @msg + 'Starting Schools Sync' + CHAR(13) + CHAR(10);

	BEGIN TRY
		MERGE dbo.Schools AS T
		USING(
			SELECT dc.Id AS CollegeId, ss.*
			FROM Staging.Schools ss
			LEFT JOIN dbo.Colleges dc
			ON ss.CollegeOrgCode = dc.OrganisationCode
			WHERE dc.Id IS NOT NULL
		) AS S
		ON T.OrganisationCode=S.SchoolOrgCode
		WHEN NOT MATCHED BY TARGET THEN
		INSERT (id, CollegeId, OrganisationCode, OrganisationDescription, HeadOfSchools, isActive)
		VALUES(S.FakeId+@maxid, S.CollegeId, S.SchoolOrgCode, S.SchoolOrg, S.HeadOfSchool, 1)
		WHEN MATCHED AND (T.OrganisationCode<>S.SchoolOrgCode OR T.HeadOfSchools<>S.HeadOfSchool OR T.CollegeId<>S.CollegeId ) THEN
		UPDATE SET 
			OrganisationDescription = S.SchoolOrg,
			HeadOfSchools = S.HeadOfSchool,
			CollegeId = S.CollegeId
		WHEN NOT MATCHED BY SOURCE AND T.isActive=1 THEN
		UPDATE SET isActive=0
		OUTPUT 
			CASE
			WHEN $action='UPDATE' AND inserted.isActive=0 THEN 'MADE INACTIVE'
			WHEN $action='INSERT' THEN 'ADDED'
			ELSE $action
			END,
			inserted.id,
			inserted.OrganisationCode,
			inserted.isActive
		INTO @results;

		SELECT @records = (SELECT COUNT(*) FROM @results);
		SET @msg = @msg + CAST(@records AS varchar(6)) + ' records affected' + CHAR(13) + CHAR(10);
	END TRY
	BEGIN CATCH
		SET @msg = @msg + ERROR_MESSAGE() + CHAR(13) + CHAR(10);
		SET @retcode = ERROR_NUMBER();
	END CATCH

	SELECT [Status], SchoolId, School, isActive
	FROM @results
	UNION
	SELECT 'SKIPPED', NULL, ss.SchoolOrgCode, NULL
	FROM Staging.Schools ss
	LEFT JOIN dbo.Colleges dc
	ON ss.CollegeOrgCode = dc.OrganisationCode
	WHERE dc.Id IS NULL;

	RETURN @retcode;
END
GO

IF OBJECT_ID('dbo.syncStaffFromStaging') IS NOT NULL DROP PROCEDURE dbo.syncStaffFromStaging
GO

CREATE PROCEDURE dbo.syncStaffFromStaging(@msg nvarchar(4000) out, @records int out)
AS
BEGIN
	DECLARE @results TABLE (
		[Status] varchar(15),
		StaffMemberId int,
		PayrollNo int,
		OldSchool int,
		NewSchool int,
		OldAppointmentStartDate datetime,
		NewAppointmentStartDate datetime,
		OldAppointmentEndDate datetime,
		NewAppointmentEndDate datetime,
		OldAppointmentExpectedEndDate datetime,
		NewAppointmentExpectedEndDate datetime,
		OldCategory nvarchar(10),
		NewCategory nvarchar(10),
		OldGroup nvarchar(10),
		NewGroup nvarchar(10),
		OldGrade nvarchar(10),
		NewGrade nvarchar(10),
		OldFte nvarchar(15),
		NewFte nvarchar(15),
		OldSalary nvarchar(150),
		NewSalary nvarchar(150),
		isActive bit
	);

	DECLARE @filter TABLE (	-- for extracting most recent staff member record where there are valid duplicates
		distinctid int
	)

	DECLARE @dupcount int=0, @retcode int=0, @maxid int;

	SET NOCOUNT ON;

	SET @msg = ISNULL(@msg, '');
	SET @msg = @msg + 'Starting Staff Members Sync' + CHAR(13) + CHAR(10);

	SELECT @maxid = MAX(id) FROM dbo.StaffMembers;
	SET @maxid = ISNULL(@maxid, 0);

	INSERT INTO Staging.BadStaffData 
	(PersonCode, Forename, Surname, AppointmentExpectedEndDate, AppointmentStartDate, ExpectedEndDate, EndDate, StartDate, VisaStart, VisaEnd, LtrEndDate, LtrLastChecked, LtrStartDate, LastUpdated)
	SELECT
		PersonCode, Forename, Surname, AppointmentExpectedEndDate, AppointmentStartDate, ExpectedEndDate, EndDate, StartDate, VisaStart, VisaEnd, LtrEndDate, LtrLastChecked, LtrStartDate, LastUpdated
	FROM Staging.StaffMembers
	WHERE TRY_CAST(ISNULL(ExpectedEndDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(StartDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(EndDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(VisaStart, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(VisaEnd, '2000-01-01') AS datetime) IS NULL

	DELETE FROM Staging.StaffMembers
	WHERE TRY_CAST(ISNULL(ExpectedEndDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(StartDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(EndDate, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(VisaStart, '2000-01-01') AS datetime) IS NULL
	OR TRY_CAST(ISNULL(VisaEnd, '2000-01-01') AS datetime) IS NULL

	SELECT @dupcount = COUNT(*) FROM (
		SELECT PersonCode, ISNULL(EndDate, '2099-12-31') ed
		FROM Staging.StaffMembers
		GROUP BY PersonCode, ISNULL(EndDate, '2099-12-31')
		HAVING COUNT(*)>1
	) AS v;

	IF @dupcount > 0 
	BEGIN
		SET @msg = @msg + CAST(@dupcount AS varchar(6)) + 'duplicates found.  Sync Aborted' + CHAR(13) + CHAR(10);
		
		SELECT 'DUPLICATES' AS Status, PersonCode
		FROM Staging.StaffMembers
		GROUP BY PersonCode, ISNULL(EndDate, '2099-12-31')
		HAVING COUNT(*)>1;

		RETURN 50099
	END

	INSERT INTO @filter
	SELECT DISTINCT LAST_VALUE(fakeid) over (PARTITION BY PersonCode order by isnull(EndDate, '2099-12-31')  ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING )
	from Staging.StaffMembers;

	BEGIN TRY
		MERGE dbo.StaffMembers T 
		USING (
			SELECT st.*, sc.id AS SchoolId
			FROM Staging.StaffMembers st
			INNER JOIN @filter f
			on st.fakeid=f.distinctid
			LEFT JOIN dbo.Schools sc
			ON st.SchoolOrganisationCode = sc.OrganisationCode
			WHERE sc.id IS NOT NULL
		) AS S
		ON T.PayrollNo = S.PersonCode
		WHEN NOT MATCHED BY TARGET AND dbo.getImmigrationStatus(s.ImmigrationStatus)<>0 THEN 
		INSERT (
			id, 
			PayrollNo, 
			SchoolId, 
			AppointmentStartDate, 
			AppointmentEndDate, 
			AppointmentExpectedEndDate, 
			[Category], 
			FirstName, 
			ImmigrationStatus, 
			JobTitle, 
			LastName, 
			NationalInsuranceNumber, 
			[Group], 
			Grade, 
			Fte, 
			Salary, 
			VisaNo, 
			VisaStart,
			VisaEnd,
			HasAttendanceMonitoredToDate, 
			IsRequiredToMonitorAttendance, 
			IsApproved, 
			Discriminator, 
			isActive
		) VALUES (
			S.fakeid+@maxid,
			S.PersonCode, 
			S.SchoolId, 
			CAST(S.StartDate AS datetime), 
			CAST(S.EndDate AS datetime), 
			CAST(S.ExpectedEndDate AS datetime) ,
			S.StaffCategory, 
			S.Forename, 
			dbo.getImmigrationStatus(s.ImmigrationStatus), 
			s.JobTitle, 
			S.Surname, 
			S.NINumber, 
			S.StaffGroup, 
			S.StaffGrade, 
			S.FTE, 
			S.Salary, 
			S.VisaNo, 
			CAST(S.VisaStart AS datetime),
			CAST(S.VisaEnd AS datetime),
			0, 0, 1, 'StaffMember', 1
		)
		WHEN MATCHED AND dbo.getImmigrationStatus(s.ImmigrationStatus)<>0 THEN
		UPDATE SET
			SchoolId=S.SchoolId,
			AppointmentStartDate=S.StartDate,
			AppointmentEndDate=S.EndDate, 
			AppointmentExpectedEndDate=S.ExpectedEndDate,
			[Category]=S.StaffCategory,
			[Group]=S.StaffGroup,
			Grade = S.StaffGrade,
			Fte = S.FTE,
			Salary = REPLACE(S.Salary, '&pound;', '£'),
			FirstName = S.Forename,
			ImmigrationStatus = dbo.getImmigrationStatus(s.ImmigrationStatus),
			JobTitle = S.JobTitle,
			LastName = S.Surname,
			VisaNo = S.VisaNo
		WHEN NOT MATCHED BY SOURCE AND T.isActive=1 THEN
		UPDATE SET isActive = 0
		OUTPUT 
			CASE
			WHEN $action='UPDATE' AND inserted.isActive=0 THEN 'MADE INACTIVE'
			WHEN $action='INSERT' THEN 'ADDED'
			ELSE $action
			END,
			inserted.id,
			inserted.PayrollNo,
			deleted.SchoolId,
			inserted.SchoolId,
			deleted.AppointmentStartDate,
			inserted.AppointmentStartDate,
			deleted.AppointmentEndDate,
			inserted.AppointmentEndDate,
			deleted.AppointmentExpectedEndDate,
			inserted.AppointmentExpectedEndDate,
			deleted.[Category],
			inserted.[Category],
			deleted.[Group],
			inserted.[Group],
			deleted.Grade,
			inserted.Grade,
			deleted.Fte,
			inserted.Fte,
			deleted.Salary,
			inserted.Salary,
			inserted.isActive
		INTO @results;

		SELECT @records = (SELECT COUNT(*) FROM @results);		-- may not be relevant since records will be updated whether or not changes have been made
		SET @msg = @msg + CAST(@records AS varchar(6)) + ' records affected' + CHAR(13) + CHAR(10);
	END TRY
	BEGIN CATCH
		SET @retcode = ERROR_NUMBER();
		SET @msg = @msg + ERROR_MESSAGE() + CHAR(13) + CHAR(10);

	END CATCH

	INSERT INTO @results ([Status], PayrollNo) 
	SELECT 'SKIPPED', PersonCode
	FROM Staging.StaffMembers st
	LEFT JOIN dbo.Schools sc
	ON st.SchoolOrganisationCode = sc.OrganisationCode
	WHERE sc.id IS NULL	
			
	SELECT * FROM @results;

	INSERT INTO Staging.StaffUpdateLog (
		UpdateReference, 
		[Status], 
		StaffMemberId, PayrollNo, 
		OldSchool, 
		NewSchool, 
		OldAppointmentStartDate,
        NewAppointmentStartDate,
        OldAppointmentEndDate,
        NewAppointmentEndDate,
		OldAppointmentExpectedEndDate,
		NewAppointmentExpectedEndDate,
		OldCategory,
		NewCategory,
		OldGroup,
		NewGroup,
		OldGrade,
		NewGrade,
		OldFte,
		NewFte,
		OldSalary,
		NewSalary,
		isActive
	)
	SELECT 
		CONVERT(varchar(8), GETDATE(), 112) + RIGHT(REPLICATE('0',4) + CAST(ROUND(RAND()*9999, 0) as varchar(4) ), 4), 
			[Status]
           ,StaffMemberId
           ,PayrollNo
           ,OldSchool
           ,NewSchool
           ,OldAppointmentStartDate
           ,NewAppointmentStartDate
           ,OldAppointmentEndDate
           ,NewAppointmentEndDate
           ,OldAppointmentExpectedEndDate
           ,NewAppointmentExpectedEndDate
           ,OldCategory
           ,NewCategory
           ,OldGroup
           ,NewGroup
           ,OldGrade
           ,NewGrade
           ,OldFte
           ,NewFte
           ,OldSalary
           ,NewSalary
           ,isActive
	FROM @results; 

	RETURN @retcode;
END
go

IF OBJECT_ID('dbo.PopulateStaffMemberAttendances') IS NOT NULL DROP PROCEDURE dbo.PopulateStaffMemberAttendances;
GO

CREATE PROCEDURE dbo.PopulateStaffMemberAttendances(@msg nvarchar(4000) OUTPUT, @records int OUTPUT) AS
BEGIN
	DECLARE @retcode int = 0;
	Declare @StaffWithMissingAttendance table(StaffMemberId int not null, AttendanceId int);

	DECLARE @seed int=1;
	DECLARE sma_cursor CURSOR READ_ONLY FOR
		SELECT saw.StaffMemberId, saw.AttendanceId
		FROM vwStaffAttendanceWeeks saw
		LEFT JOIN StaffMemberAttendances sma
		on saw.StaffMemberId=sma.StaffMemberId 
		AND saw.AttendanceId=sma.AttendanceId
		WHERE sma.Id IS NULL;
	DECLARE @vStaffMemberId int, @vAttendanceId int;

	SET @msg = ISNULL(@msg, '');
	SET @records = 0;

	SET NOCOUNT ON;

	-- StaffMemberAttendances.id is required, and not defined as an auto-increment
	-- have to insert each row individually, in order to set unique ids
	
	IF (SELECT COUNT(id) FROM StaffMemberAttendances) > 0
	BEGIN
		SELECT @seed = MAX(id) + 1 FROM StaffMemberAttendances;
	END
	BEGIN TRANSACTION;

	BEGIN TRY
		--INSERT INTO @StaffWithMissingAttendance(StaffMemberId, AttendanceId)
		--SELECT saw.StaffMemberId, saw.AttendanceId
		--FROM vwStaffAttendanceWeeks saw
		--LEFT JOIN StaffMemberAttendances sma
		--on saw.StaffMemberId=sma.StaffMemberId 
		--AND saw.AttendanceId=sma.AttendanceId
		--WHERE sma.Id IS NULL;

		OPEN sma_cursor;
		WHILE 1=1
		BEGIN
			FETCH NEXT FROM sma_cursor INTO @vStaffMemberId, @vAttendanceId;
			IF @@FETCH_STATUS<>0 BREAK;

			INSERT INTO @StaffWithMissingAttendance
			(StaffMemberId, AttendanceId)
			VALUES(@vStaffMemberId, @vAttendanceId);

			INSERT INTO dbo.StaffMemberAttendances 
			(id, StaffMemberId, AttendanceId, IsLeaving, AttendanceType)
			VALUES (@seed + ROUND(RAND()*25, 0), @vStaffMemberId, @vAttendanceId, 0, 4);	-- Attendance Type 4 = absence
		END
		CLOSE sma_cursor;

		SET @records = @records + @@ROWCOUNT;

		INSERT INTO dbo.StaffMemberAttendanceCompletionDatas
		([DayOfWeek], StaffMemberId, AttendanceId, IsComplete)
		SELECT sawu.[DayOfWeek], sawu.StaffMemberId, sawu.AttendanceId, 0
		FROM (
			SELECT 1 as [DayOfWeek], StaffMemberId, AttendanceId
			FROM vwStaffAttendanceWeeks
			UNION ALL
			SELECT 2, StaffMemberId, AttendanceId
			FROM vwStaffAttendanceWeeks
			UNION ALL
			SELECT 3, StaffMemberId, AttendanceId
			FROM vwStaffAttendanceWeeks
			UNION ALL
			SELECT 4, StaffMemberId, AttendanceId
			FROM vwStaffAttendanceWeeks
			UNION ALL
			SELECT 5, StaffMemberId, AttendanceId
			FROM vwStaffAttendanceWeeks
		) sawu
		LEFT JOIN dbo.StaffMemberAttendanceCompletionDatas sma
		on sawu.[DayOfWeek]=sma.[DayOfWeek] 
		AND sawu.StaffMemberId=sma.StaffMemberId
		AND sawu.AttendanceId=sma.AttendanceId
		where sma.AttendanceId IS NULL ;

		SET @records = @records + @@ROWCOUNT;

		SET @msg = @msg + CAST(@records AS varchar(6)) + ' records affected' + CHAR(13) + CHAR(10);

		IF @@TRANCOUNT>0 COMMIT TRANSACTION;

	END TRY
	BEGIN CATCH
		SET @retcode = ERROR_NUMBER();
		SET @msg = ERROR_MESSAGE() + CHAR(13) + CHAR(10);

		IF @@TRANCOUNT>0 ROLLBACK TRANSACTION;
	END CATCH

	SELECT * FROM @StaffWithMissingAttendance;

	RETURN @retcode;
END

GO

