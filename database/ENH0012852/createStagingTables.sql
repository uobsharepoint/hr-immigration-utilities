USE [hr_immigration]
GO

IF SCHEMA_ID('Staging') IS NULL	EXEC('CREATE SCHEMA Staging AUTHORIZATION dbo');
GO

IF OBJECT_ID('Staging.Colleges') IS NOT NULL DROP TABLE Staging.Colleges;
GO

CREATE TABLE Staging.Colleges (
	fakeid int IDENTITY(1,1),
	CollegeOrgCode nvarchar(20),
	CollegeOrg nvarchar(100),
	LastUpdated datetime default(CURRENT_TIMESTAMP)
);
GO

IF OBJECT_ID('Staging.Schools') IS NOT NULL DROP TABLE Staging.Schools;
GO

CREATE TABLE Staging.Schools (
	fakeid int IDENTITY(1,1),
	CollegeOrgCode nvarchar(20),
	SchoolOrgCode nvarchar(20),
	SchoolOrg nvarchar(100),
	HeadOfSchool nvarchar(200),
	LastUpdated datetime default(CURRENT_TIMESTAMP)
);
go


IF OBJECT_ID('Staging.ApplicationUsers') IS NOT NULL DROP TABLE Staging.ApplicationUsers;
GO

CREATE TABLE Staging.ApplicationUsers(
	Id int IDENTITY(1,1) NOT NULL,
	UserName nvarchar(256)  NULL,
	FirstName nvarchar(max) NULL,
	LastName nvarchar(max) NULL,
	Role int NULL,
	IsActive bit NULL,
	IsSystemAdmin bit NULL)

IF OBJECT_ID('Staging.StaffMembers') IS NOT NULL DROP TABLE Staging.StaffMembers;
GO

CREATE TABLE Staging.StaffMembers (
	fakeid int IDENTITY(1,1),
	ImmigrationStatus int,
	ImmigrationStatusDesc nvarchar(20),
	PersonCode int,
	NiNumber nvarchar(15),
	ExpectedEndDate char(10),	-- EXPECTED_END_DATE
	Forename nvarchar(100),
	Surname nvarchar(100),
	EndDate char(10),		-- APPOINTMENT_START_DATE ?
	StartDate char(10),		-- APPOINTMENT_END_DATE ?
	JobTitle nvarchar(100),
	AppointedOrgCode nvarchar(20),	
	AppointedOrg nvarchar(100),
	SchoolOrganisationCode nvarchar(20),
	SchoolOrganisationDesc nvarchar(100),
	College nvarchar(100),
	Salary nvarchar(150),
	FTE nvarchar(15),
	VisaNo nvarchar(20),
	VisaStart char(10),
	VisaEnd char(10),
	StaffCategory nvarchar(10),
	StaffGroup nvarchar(10),
	StaffGrade nvarchar(10),
	COSStartDate char(10),	-- ignore ?
	COSEndDate char(10), -- ignore
	EmailAddress nvarchar(100),
	LastUpdated datetime default(CURRENT_TIMESTAMP)
);

CREATE TABLE Staging.BadStaffData (
	PersonCode int,
	Forename nvarchar(100),
	Surname nvarchar(100),
	ExpectedEndDate char(10),
	EndDate char(10),
	StartDate char(10),
	VisaStart char(10),
	VisaEnd char(10),
	LastUpdated datetime 
);

GO

IF OBJECT_ID('Staging.StaffUpdateLog') IS NOT NULL DROP TABLE Staging.StaffUpdateLog;
GO

CREATE TABLE Staging.StaffUpdateLog (
	id int IDENTITY(1,1) PRIMARY KEY,
	UpdateReference char(12) NOT NULL,
	[Status] varchar(15) NOT NULL,
	PayrollNo int NOT NULL,
	StaffMemberId int NULL,
	OldSchool int,
	NewSchool int,
	OldAppointmentStartDate datetime,
	NewAppointmentStartDate datetime,
	OldAppointmentEndDate datetime,
	NewAppointmentEndDate datetime,
	OldAppointmentExpectedEndDate datetime,
	NewAppointmentExpectedEndDate datetime,
	OldCategory nvarchar(10),
	NewCategory nvarchar(10),
	OldGroup nvarchar(10),
	NewGroup nvarchar(10),
	OldGrade nvarchar(10),
	NewGrade nvarchar(10),
	OldFte nvarchar(10),
	NewFte nvarchar(10),
	OldSalary nvarchar(150),
	NewSalary nvarchar(150),
	isActive bit
);
GO
